<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $modules = [
            'user',
            'role',
            'room-type'
        ];
        foreach ($modules as $module) {
            Permission::create(['guard_name' => 'api', 'name' => 'create-' . $module]);
            Permission::create(['guard_name' => 'api', 'name' => 'update-' . $module]);
            Permission::create(['guard_name' => 'api', 'name' => 'delete-' . $module]);
            Permission::create(['guard_name' => 'api', 'name' => 'read-' . $module]);
        }
    }
}
