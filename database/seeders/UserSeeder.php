<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Role::create(['guard_name' => 'api','name' => 'user']);
        $users = [
            ['name' => 'user2',
            'username' => 'user2',
            'email' => 'user2@gmail.com',
            'password' => Hash::make('12345678')],
            ['name' => 'user3',
                'username' => 'user3',
                'email' => 'user3@gmail.com',
                'password' => Hash::make('12345678')],
        ];
        foreach ($users as $user){
            $user = User::create($user);
            $user->assignRole('user');
        }
    }
}
