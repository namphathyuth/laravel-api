<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('auth/register', [\App\Http\Controllers\Api\AuthenticationController::class, 'register']);
Route::post('auth/login', [\App\Http\Controllers\Api\AuthenticationController::class, 'login']);

Route::middleware('auth:api')->group(function (){
  Route::get('auth/profile', [\App\Http\Controllers\Api\AuthenticationController::class, 'profile']);
  Route::post('auth/logout', [\App\Http\Controllers\Api\AuthenticationController::class, 'logout']);
  Route::resource('user', \App\Http\Controllers\Api\UserController::class)->missing(function (Request $request) {
            return response()->json(['message' => 'not found'], 404);
  })->withTrashed()->middleware(['permission:create-user|update-user|delete-user|read-user']);
});

Route::middleware('auth:api')->group(function (){
    Route::get('user', [\App\Http\Controllers\Api\UserController::class, 'index'])->middleware(['permission:read-user']);
    Route::post('user', [\App\Http\Controllers\Api\UserController::class, 'store'])->middleware(['permission:create-user']);
    Route::delete('user/{id}', [\App\Http\Controllers\Api\UserController::class, 'destroy'])->middleware(['permission:delete-user']);
    Route::patch('user', [\App\Http\Controllers\Api\UserController::class, 'update'])->middleware(['permission:update-user']);
    Route::get('user/{id}', [\App\Http\Controllers\Api\UserController::class, 'show'])->middleware(['permission:read-user']);
});

Route::middleware('auth:api')->group(function (){
    Route::post('room-type', [\App\Http\Controllers\Api\RoomTypeController::class, 'store'])->middleware(['permission:create-room-type']);
    Route::get('room-type', [\App\Http\Controllers\Api\RoomTypeController::class, 'index'])->middleware(['permission:read-room-type']);
    Route::delete('room-type/{id}', [\App\Http\Controllers\Api\RoomTypeController::class, 'destroy'])->middleware(['permission:delete-room-type']);
    Route::get('room-type/{id}', [\App\Http\Controllers\Api\RoomTypeController::class, 'show'])->middleware(['permission:read-room-type']);
    Route::patch('room-type/{id}', [\App\Http\Controllers\Api\RoomTypeController::class, 'update'])->middleware(['permission:update-room-type']);
});

Route::middleware('auth:api')->group(function (){
    Route::get('role', [\App\Http\Controllers\Api\RoleController::class, 'index'])->middleware(['permission:read-role']);
    Route::post('role', [\App\Http\Controllers\Api\RoleController::class, 'store'])->middleware(['permission:create-role']);
    Route::delete('role/{id}', [\App\Http\Controllers\Api\RoleController::class, 'destroy'])->middleware(['permission:delete-role']);
    Route::get('role/{id}', [\App\Http\Controllers\Api\RoleController::class, 'show'])->middleware(['permission:read-role']);
    Route::patch('role/{id}', [\App\Http\Controllers\Api\RoleController::class, 'update'])->middleware(['permission:update-role']);
    Route::post('role/assign/{id}', [\App\Http\Controllers\Api\RoleController::class, 'assignPermissionToRole'])->middleware(['permission:create-role']);
    Route::get('permission', [\App\Http\Controllers\Api\PermissionController::class, 'index'])->middleware(['permission:read-role']);
});
