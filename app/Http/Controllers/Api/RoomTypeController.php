<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\Room;
use App\Models\RoomType;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RoomTypeController extends Controller
{
    public function store(Request $request): JsonResponse
    {
       $validation = $request->validate([
           'title' => 'required|string|max:255',
           'description' => 'required|string',
           'num_of_bed' => 'required|integer',
           'num_of_people' => 'required|integer',
           'price' => 'required|numeric',
           'image' => 'required|array',
           'image.*' => 'required|image',
       ]);
       $roomType = RoomType::create($validation);
       foreach ($validation['image'] as $imageFile){
           $imagePath = $imageFile->store('image', 'public');
           $roomType->image()->save(new Image(['path' => $imagePath]));
       }

       return response()->json([], 201);
    }

    public function index(Request $request): JsonResponse
    {
        $roomType = RoomType::with('image');
        if($request->get('query'))
            $roomType->with('image')->where('title', 'like', '%' . $request->get('query') . '%');
        if($request->get('bed'))
            $roomType->with('image')->where('num_of_bed', $request->get('bed'));
        if($request->get('people-number'))
            $roomType->with('image')->where('num_of_people', $request->get('people-number'));
        return response()->json(['rooms' => $roomType->paginate($request->get('perPage', 10))], 200);
    }

    public function destroy($id): JsonResponse
    {
        RoomType::destroy($id);
        return response()->json([], 204);
    }

    public function show($id): JsonResponse
    {
       $roomType = RoomType::with('image')->where('id', $id)->first();
       return response()->json(['rooms' => $roomType], 200);
    }

    public function update(Request $request, $id): JsonResponse
    {
        $validated = $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'num_of_bed' => 'required|integer',
            'num_of_people' => 'required|integer',
            'price' => 'required|numeric',
            'image' => 'array',
            'image.*' => 'required|image',
            'image_to_remove' => 'array',
            'image_to_remove.*' => 'string',

        ]);
        $roomType = RoomType::findOrFail($id);
        $roomType->update($request->all());
        if($request->hasFile('image')) {
            foreach ($validated['image'] as $imageFile) {
                $imagePath = $imageFile->store('image', 'public');
                $roomType->image()->save(new Image(['path' => $imagePath]));
            }
        }
        if($request->has('image_to_remove')){
            foreach ($validated['image_to_remove'] as $imagePath){
                $image = Image::where('path', $imagePath)->firstOrFail();
                if($image->room_type_id == $roomType->id){
                    $image->delete();
                    if(Storage::exists($imagePath))
                        Storage::delete($imagePath);
                }
            }
        }
        return response()->json([], 200);

    }
}
