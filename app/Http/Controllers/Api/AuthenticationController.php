<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class AuthenticationController extends Controller
{
    public function register(Request $request): JsonResponse{
        $this->validate($request,[
            'name' => 'required',
            'username' => 'required|string|max:255|unique:' . User::class,
            'email' => 'required|string|lowercase|email|max:255|unique:' . User::class,
            'password' => ['required', 'confirmed', Password::default()]
        ]);

        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
        $user->assignRole('user');

        $token = $user->createToken('Laravel10PassportAuth')->accessToken;
        return response()->json(['token' => $token], 200);
    }

    public  function login(Request $request): JsonResponse {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if(auth()->attempt($data)){
            $user = auth()->user();
            $token = $user->createToken('access token')->accessToken;
            $roles = $user->roles()->pluck('name')->toArray();
            return response()->json(['user' => $user, 'roles' => $roles, 'token' => $token], 200);
        }else return response()->json(['error' => 'Incorrect credential'], 401);
    }
     public function profile(): JsonResponse
     {
        $user = auth()->user();
        $roles = $user->roles()->pluck('name')->toArray();
        return response()->json(['user' => $user, 'roles' => $roles]);
     }

     public function logout(Request $request): JsonResponse
     {
         $request->user()->token()->revoke();
         return response()->json();
     }
}
