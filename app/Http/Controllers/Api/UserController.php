<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): JsonResponse
    {
        $users = User::with(['roles', 'roles.permissions'])->withoutRole('admin');
        if($request->get('name'))
            $users->with(['roles', 'roles.permissions'])->where('name', 'like', '%' . $request->get('name') . '%');
        if ($request->get('role'))
            $users->with(['roles', 'roles.permissions'])->whereHas('roles', function ($query, $request){
                $query->where('name', $request->get('role'));
            });
        return response()->json(['users' => $users->paginate($request->get('perPage', 10))], 200);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'username' => 'required|string|max:255|unique:' . User::class,
            'email' => 'required|string|lowercase|email|max:255|unique:' . User::class,
            'password' => ['required', 'confirmed', Password::default()],
            'role' => 'string|max:50|required'
        ]);

        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
        $user->assignRole($request->role);
        return response()->json([],201);
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id): JsonResponse
    {
        $user = User::with(['roles', 'roles.permissions'])->where('id', $id)->first();
        return response()->json(['user' => $user], 200);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, int $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'username' => ['required', 'max:255', 'string', Rule::unique(User::class)->ignore($id)],
            'email' => ['required', 'string', 'lowercase', 'email', 'max:255', Rule::unique(User::class)->ignore($id)],
            'role' => 'required|string|max:50'
        ]);
        $user = User::findOrFail($id);
        $user->fill($request->all())->save();
        $user->syncRoles([$request->role]);
        return response()->json([],200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): JsonResponse
    {
        User::destroy($id);
        return response()->json([], 204);
    }
}
