<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function store(Request $request): JsonResponse
    {
        $validated = $request->validate([
            'name' => 'required|string|max:50|unique:' . Role::class
        ]);
        Role::create(['guard_name' => 'api', 'name' => $validated['name']]);
        return response()->json([], 201);
    }
    public function index(Request $request): JsonResponse
    {
        $roles = Role::with('permissions');
        if($request->get('query')){
            $roles->with('permissions')->where('name', 'like', '%' . $request->get('query') . '%');
        }
        return response()->json(['roles' => $roles->paginate($request->get('perPage', 10))], 200);
    }
    public function show(Request $request, $id)
    {
        $role = Role::findOrFail($id)->first();
        return response()->json(['role' => $role], 200);
    }
    public function destroy($id): JsonResponse
    {
       Role::destroy($id);
       return response()->json([], 204);
    }

    public function update(Request $request, int $id): JsonResponse
    {
       $validated = $request->validate([
           'name' => ['string', 'required', 'max:50', Rule::unique(Role::class)->ignore($id)]
       ]);
       $role = Role::findOrFail($id);
       $role->update($validated);
       return response()->json([], 200);
    }
    public function assignPermissionToRole(Request $request, int $id): JsonResponse
    {
        $validated = $request->validate([
           'permissions' => 'array'
        ]);
        $role = Role::findOrFail($id);
        $permissions = [];
        $removePermissions = [];
        foreach ($request['permissions'] as $permission){
            if($permission['status'])
               $permissions[] = $permission;
            else $removePermissions[] = $permission;
        }
        $role->syncPermissions($permissions);
        return response()->json([], 200);
    }
}
