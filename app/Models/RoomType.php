<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomType extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'room_type';
    protected $fillable = [
        'title',
        'description',
        'num_of_bed',
        'num_of_people',
        'price',
    ];

    protected $hidden = [
        'room_type_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    public function room(): HasMany
    {
       return $this->hasMany(Room::class);
    }
    public function image(): HasMany
    {
       return $this->hasMany(Image::class);
    }
}
